package main

import (
	"net/http"
	"time" 
	"log"
	"github.com/gorilla/mux"
	"strconv"
	"encoding/json"
	"io/ioutil"
)

func main(){
	ChessScore = &Score{P1:0,P2:0}
	startServer()
}

type Score struct{
	P1 int
	P2 int 
}

type ScoreJson struct{
	Scored string `json:"scored"`
}

var ChessScore *Score


func startServer(){
	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/puzzle", handleGetPuzzle).Methods("GET")
	r.HandleFunc("/puzzle", handlePostPuzzle).Methods("POST").Headers("Content-Type", "application/json")

	r.HandleFunc("/chess", handleGetChess).Methods("GET")
	r.HandleFunc("/chess", handlePostChess).Methods("POST").Headers("Content-Type", "application/json")

	server := &http.Server{
			Addr       : "localhost:8086",
			IdleTimeout: duration,
			Handler    : r, 
	}
	log.Println("[INFO] Server HTTP Online! Adress ---->http://"+ server.Addr + ".")
	log.Fatal(server.ListenAndServe())
}

func handleGetPuzzle(res http.ResponseWriter, req *http.Request){
	res.Write([]byte("Puzzle"))
}
func handlePostPuzzle(res http.ResponseWriter, req *http.Request){
	res.Write([]byte("Puzzle"))
}

func handleGetChess(res http.ResponseWriter, req *http.Request){
	body := []byte(`{ "P1Score": "` + strconv.Itoa(ChessScore.P1) + `", "P2Score": "`+
	strconv.Itoa(ChessScore.P2)+`" }`)
	res.Write(body)
}
func handlePostChess(res http.ResponseWriter, req *http.Request){
	var s ScoreJson
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &s)

	if s.Scored != "P1" && s.Scored != "P2"{
		res.Write([]byte("BAD REQUEST"))
		return
	}
	if s.Scored == "P1"{
		ChessScore.P1 = ChessScore.P1 + 1
		return
	}
	ChessScore.P2 = ChessScore.P2 + 1


	
}